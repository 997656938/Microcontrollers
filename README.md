# Microcontrollers

## 软件


- Visual Studio 2015
[官网](https://www.visualstudio.com/)
[百度百科](http://baike.baidu.com/view/261613.htm)

- VAssist X 2102
[官网](http://www.wholetomato.com/)
[百度百科](http://baike.baidu.com/view/4439152.htm)

- Keil uVision5 (keil C51 V9.55)
[百度百科](http://baike.baidu.com/view/8583817.htm)
[官网](http://www.keil.com/download/product/)

## 参考资料


教你利用Visual Studio的智能提示，快速编写C51代码
http://www.amobbs.com/thread-4080638-1-1.html
