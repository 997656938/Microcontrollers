#include<reg51.h>
#include<intrins.h>

void Delay10ms(int c);


void led_006();
void led_005();
void led_007();
 
sbit K1=P0^0;
void main(void)
{
led_007();
}


//按键控制LED
void led_007()
{
		int i=0,j;
 
	j=0x01; 
 
	while(1)
	{
		P2=j;
		if((P0^0)==0)
		{
		Delay10ms(1);
			if((P0^0)==0)
			{
				j=_cror_(j,1);
			 while((i<50)&&((P0^0)==0))
				 {
			 	Delay10ms(1);
				 	i++;
				 }
			 	i=0;
			}
		}
	}
}	


//按键控制LED
void led_006()
{
		int i=0,j;
 
	j=0x01; 
 
	while(1)
	{
		P2=j;
		if(K1==0)
		{
		Delay10ms(1);
			if(K1==0)
			{
				j=_cror_(j,1);
			 while((i<50)&&(K1==0))
				 {
			 	Delay10ms(1);
				 	i++;
				 }
			 	i=0;
			}
		}
	}
}	












//第几盏隔几秒
void led_005()
	{
	unsigned char n;

	int n0=0,n1=0,n2=0,n3=0,n4=0,n5=0,n6=0,n7=0;
		
	while(1)
	{
		// 1 2 3 4 5 6 7 8 9 最小公倍数=5*7*8*9= 2520 
		int i ;
		for( i=0;i<2520;i++)
		{
			n0=((i%2==0)?1:0);
			n1=(i%3==0)?1:0;
			n2=(i%4==0)?1:0;
			n3=(i%5==0)?1:0;
			n4=(i%6==0)?1:0;
			n5=(i%7==0)?1:0;
			n6=(i%8==0)?1:0;
			n7=(i%9==0)?1:0;
			 
		 P2 = (1 << 0)*n0 + (1 << 1)*n1 + (1 << 2)*n2 + (1 << 3)*n3 + (1 << 4)*n4 + (1 << 5)*n5 + (1 << 6)*n6 + (1 << 7)*n7;
		 Delay10ms(200); 			
		}

		 
	}
}
 








//亮两盏中间隔一盏灯，循环跑
void led_004()
	{
	unsigned char n;

	while(1)
	{
		P2=5;
		Delay10ms(200); 
		for(n=0;n<5;n++)
		{
			P2= P2 << 1; 
			Delay10ms(200);
		}
		P2=65;
		Delay10ms(200);
		P2=130;
		Delay10ms(200);
	}
}


//亮两盏中间隔一盏灯，来回跑
void led_003(){
	unsigned char n;
	P2=5;  
	while(1)
	{
		for(n=0;n<5;n++)
		{
			P2= P2 << 1; 
			Delay10ms(200);
		}
		for(n=0;n<5;n++)
		{
			P2= P2 >> 1; 
			Delay10ms(200);
		}
	}
}




//亮两盏灯来回跑
void led_002(){
	unsigned char n;
	P2=3;  
	while(1)
	{
		for(n=0;n<6;n++)
		{
			P2= P2 << 1; 
			Delay10ms(200);
		}
		for(n=0;n<6;n++)
		{
			P2= P2 >> 1; 
			Delay10ms(200);
		}
	}
}












//亮一盏灯来回跑
void led_001()
	{
	unsigned char n;
	P2=1;  
	while(1)
	{
		for(n=0;n<7;n++)
		{
			P2= P2 << 1; 
			//P2=_crol_(P2,1);
			Delay10ms(200);
		}
		for(n=0;n<7;n++)
		{
			P2= P2 >> 1; 
			Delay10ms(200);
		}
	}
}






//延时函数   延时c*10ms
void Delay10ms(int c)   //误差 0us    
{
    unsigned char a, b;

	//--c已经在传递过来的时候已经赋值了，所以在for语句第一句就不用赋值了--//
    for (;c>0;c--)
	{
		for (b=38;b>0;b--)
		{
			for (a=130;a>0;a--);
		}
           
	}
        
}